#! /bin/bash

#e.g.
# ./all_chroms_parallel.sh 3 out ./scripts/simulate_chromosomes.py ./directory/genome.fa '--n-pops=2 --eff-sizes=2e3,750 --n-samples=10 --recomb-r=2e-8 --mut-r=7e-8 --mig-r=1e-3'

merge_vcfs_scr=$(dirname $0)/merge_vcfs.py

# Check python, sem & the genome index.
#python3 -c 'from msprime import check_numpy' || exit
command -v parallel >/dev/null || { parallel --version; exit; }
[[ -e "$merge_vcfs_scr" ]] || { ls -L "$merge_vcfs_scr"; exit; }

# Check arguments.
usage="\
Usage:
  $(basename "${BASH_SOURCE[0]}}") -o out_dir -r ref_fasta -c chrom_list [-j n_cpu] [--indel-prob=0.0] /path/to/msprime_script.py \"--msp-opt=val [...]\"

The msprime script must support options -c and -l (chromosome name and length)
and -o (output path). The msprime options that should be passed are the rates;
the wrapper loops over chromosomes and creates output names automatically.
"
short_opts='o:r:j:c:'
long_opts='indel-prob:'
eval "set -- $(getopt -o $short_opts --long $long_opts  -- "$@")"
while true ; do
    case $1 in
        -o) out_d=${2/%\//}; shift 2 ;;
        -r) genome_fa=$2; shift 2 ;;
        -c) chrom_list=$2; shift 2;;
        -j) n_cpu=$2; shift 2 ;;
        --indel-prob) indel_p=$2; shift 2 ;;
        --) shift; break ;;
        *) echo "Error: Bad argument '$1'."; exit 1;;
    esac
done
msp_scr=$1
msp_args="$2"
fai=${genome_fa/%.gz/}.fai
[[ "$n_cpu" ]] || n_cpu=1

if [[ ! "$out_d" ]] \
        || [[ ! "$genome_fa" ]] \
        || [[ ! "$chrom_list" ]] \
        || [[ ! "$msp_scr" ]] \
        || [[ ! "$msp_args" ]] \
        ;then
    echo -n "$usage";
    exit 1
elif [[ ! -e "$genome_fa" ]]; then
    ls -L "$genome_fa"
    exit 1
elif [[ ! -e "$chrom_list" ]]; then
    ls -L "$chrom_list"
    exit 1
elif [[ ! -e "$fai" ]]; then
    ls -L "$fai"
    exit 1
elif [[ ! -e "$msp_scr" ]]; then
    ls -L "$msp_scr"
    exit 1
fi

# Run the simulations.
mkdir -p $out_d/msp_vcfs || exit
rm -f $out_d/msp_vcfs/*
echo "Running simulations..."
{ { exec 3>&1 1>&4
cat "$chrom_list" |
while read -r chr_name ;do
    len=$(grep "^$chr_name\\b" "$fai" | cut -f2)
    cmd="$msp_scr -c$chr_name -l$len -o$out_d/msp_vcfs $msp_args"
    echo "$cmd"
    echo "$cmd" >&3
done
} | parallel -j $n_cpu; } 4>&1 || exit

# Write the popmap.
n_pops=$(echo "$msp_args" | grep -oE -- '--n-pops(=| +)[^ ]+' | sed -r 's/ +/=/' | cut -d '=' -f 2)
n_samples=$(echo "$msp_args" | grep -oE -- '--n-samples(=| +)[^ ]+' | sed -r 's/ +/=/' | cut -d '=' -f 2)
if [[ ! "$n_samples" =~ , ]];then
    n_samples=$(seq "$n_pops" | sed "c $n_samples" | paste -sd ',')
elif [[ $(echo "$n_samples" | tr -c -d ',' | wc -c) -ne $((n_pops-1)) ]] ;then
    echo "Oops: n_pops='$n_pops', n_samples='$n_samples'"
    exit 1;
fi
n_samples_tot=$(echo "$n_samples" | tr ',' '+' | bc)
paste \
    <(seq -w 0 $((n_samples_tot-1)) | sed 's/^/msp_/') \
    <(i=1; for n in $(echo "$n_samples" | tr ',' '\n') ;do seq $n | sed "c pop$i"; i=$((i+1)); done) \
    > "$out_d/popmap.tsv"

# Merge the VCFs.
echo "Merging VCFs..."
cmd="$merge_vcfs_scr -g $genome_fa -v $out_d/msp_vcfs/ -o $out_d/master.vcf.gz"
if [[ "$indel_p" ]] ;then
    cmd="$cmd -i $indel_p"
fi
echo "$cmd"
$cmd
