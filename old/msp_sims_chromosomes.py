#!/usr/bin/env python3

import sys, gzip, time, argparse, os
program_name = os.path.basename(__file__)

#
# set command line variables:
#
p = argparse.ArgumentParser()
p.add_argument('-f', '--fai', dest='fai')
p.add_argument('-o', '--outdir', dest='outd')
p.add_argument('-n', '--npops', dest='npops', type=int)
p.add_argument('--eff-sizes', dest='ne', type=int, action='append')
p.add_argument('--n-samples', dest='n_samples', type=int, action='append')
p.add_argument('--recomb-r', dest='recom_r', type=float)
p.add_argument('--mut-r', dest='mut_r', type=float)
p.add_argument('--mig-r', dest='mig_r', type=float)

args = p.parse_args()

# Overwrite the help/usage behavior.
p.format_usage = lambda : '''\
{prog} --fai path --outdir dir --npops int --eff-sizes int --n-samples int --recomb-r float --mut-r float --mig-r float
    
Input/Output files:
    -f, --fai:    Genome fasta index file
    -o, --outdir: Path to output directory

msprime simulation parameters:
    -n, --npops:     Number of populations to be simulated (int)
        --eff-sizes: Effective population size, one entry per population defined in \'--npops\' (int)
        --n-samples: Number of per-population samples, one entry per population defined in \'--npops\' (int)
        --recomb-r:  Recombination rate per base per generation (float)
        --mut-r:     Mutation rate per base per generation (float)
        --mig-r:     Symmetric migration rate. ~4m, where m = per generation migration rate (float)

Example:
{prog} --fai ./simulations/genome.fai --outdir ./simulations/ --npops 3 \\\n\t--eff-sizes 1500 --eff-sizes 10000 --eff-sizes 2500 \\\n\t--n-samples 15 --n-samples 15 --n-samples 20 \\\n\t--recomb-r 1e-8 --mut-r 5e-8 --mig-r 0.05
'''.format(prog=program_name)
p.format_help = p.format_usage

for required in ['fai', 'outd', 'npops', 'ne', 'n_samples', 'recom_r', 'mut_r', 'mig_r']:
    if args.__dict__[required] is None:
        print('Required argument \'{}\' is missing.\n'.format(required), file=sys.stderr)
        p.print_help()
        sys.exit(1)

if args.outd[-1] != '/':
    args.outd += '/'

# Print usae information
print(' '.join(sys.argv))