# RADsims Manual

## Introduction

## Installation

## Tutorial

### Perform coalescence simulation to generate variants

### Merge and process variants into a single master VCF

The output of the `msprime` simulations consists of per-chromosome variant tables, containing the position and per-individual genotypes of each variant encoded in a VCF-like format. The actual nature of the variant (a SNPs or an Indel, for example) and the alleles are unkown. Instead, `msprime` provides placeholder alleles, `A` and `T` for the reference and alternative allele, respectively. The `merge_vcfs.py` script can then be utilized to merge and process all the `msprime` output VCFs into a single master VCF that has all the simulated variants in the correct genome context.

`merge_vcfs.py` will correct all the reference and alternative alleles based on their identity in the reference genome. For example, if the nucleotide in position 2,345 is a `T`, then the alternative allele will be one of `A`, `C` or `G`, chosen at random. Additionally, variants will be processed as Indels at a given user-defined frequency (`-i` or `--indel_p` option). For `--indel_p=0.01`, each variant has then a 1% chance of being processed as an Indel. The length of the Indel, and its identity between an insertion or a deletion is determined at random for each Indel.

The script also merges al

### Introduce variants and extract RAD loci

### Simulate sequencing process by converting the loci into reads
