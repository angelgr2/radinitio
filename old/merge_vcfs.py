#!/usr/bin/env python3

import sys, random, re, gzip, argparse, time, os
import numpy as np

program_name = os.path.basename(__file__)
starttime = time.time()
def elapsed():
    return round(time.time()-starttime, 2)
#
# set command line variables:
#
p = argparse.ArgumentParser()
p.add_argument('-g', '--genome',  dest='geno', type=str)
p.add_argument('-v', '--vcf-dir', dest='vcfd', type=str)
p.add_argument('-o', '--outdir',  dest='outd', type=str)
p.add_argument('-i', '--indel-p', dest='indp', type=float, default=0.0)
args = p.parse_args()

# Overwrite the help/usage behavior.
p.format_usage = lambda : '''\
{prog} --genome path --vcf-dir dir --outdir dir --indel-p float

Input/Output files:
    -g, --genome: path to reference genome (gzipped fasta file)
    -v, --vcf-dir: directory of simulated per-chromosome VCFs
    -o, --outdir: path to an output directory

Additional options:
    -i, --indel-p: probability of variant being an indel

'''.format(prog=program_name)
p.format_help = p.format_usage

for required in ['geno', 'vcfd', 'outd']:
    if args.__dict__[required] is None:
        print('Required argument \'{}\' is missing.\n'.format(required), file=sys.stderr)
        p.print_help()
        sys.exit(1)

args.outd = args.outd.rstrip('/')

#
# function to change nucleotides
#
def change_nuc(seq, pos, indel_p):
    ref = seq[pos]
    i = indel_p       # probability of an indel
    s = (1 - i)/3     # probability of a substitution
    mutations = []
    if ref == 'A':
        mutations = [ 'C', 'G', 'T', 'I' ]
    elif ref == 'C':
        mutations = [ 'A', 'G', 'T', 'I' ]
    elif ref == 'G':
        mutations = [ 'A', 'C', 'T', 'I' ]
    elif ref == 'T':
        mutations = [ 'A', 'C', 'G', 'I' ]
    elif ref not in [ 'A', 'C', 'G', 'T' ]:
        mutations = [ 'N', 'N', 'N', 'N' ]
    alt = np.random.choice( mutations, p = [s, s, s, i] )
    if alt != 'I':
        return ref, alt
    else:
        indel = random.choice(['In','Del'])
        if indel == 'In':
            size = 0
            while size == 0:
                size = np.random.poisson(lam=1)
            insert = [ref]
            for i in range(size):
                n = random.choice('ACGT')
                insert.append(n)
            return ref, ''.join(insert)
        else:
            size = 0
            while size == 0:
                size = np.random.poisson(lam=1)
            d_end = pos+size+1
            if d_end <= len(seq):
                deletion = seq[pos:d_end]
                return deletion, ref
            else:
                deletion = seq[pos:len(seq)]
                return deletion, ref

#
# load the genome
#
print(elapsed(), '- Loading the genome...')
sys.stdout.flush()

GENOME = {}
seq = []
head = None
for line in gzip.open(args.geno, 'rt'):
    row = line.strip('\n')
    if row[0] == '>':
        if head != None:
            if len(''.join(seq)) >= 5000:
                # Don't add sequences if they're smaller than 5Kb
                GENOME[head] = ''.join(seq)
        regex = re.match(r'>(\S+)', line)
        #this creates a key for the dict using just the group number and not the whole fasta header
        head = regex.group(1)
        seq = []
    elif row[0] != '>':
        seq.append(row.upper())
if len(''.join(seq)) >= 5000:
    GENOME[head] = ''.join(seq)
del seq


#
# open output file
#
outfile = args.outd
out = gzip.open(outfile, 'wt')

#
# work on the vcf header
#
meta = '''##fileformat=VCFv4.2
##source=RADseq Simulations 'merge_vcfs.py'
##FILTER=<ID=PASS,Description="All filters passed">'''
out.write(meta+'\n')
meta = None
for chrom in sorted(GENOME.keys()):
    out.write('##contig=<ID={},length={}>\n'.format(chrom,len(GENOME[chrom])))
out.write('##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n')

#
# process master vcf
#
for i, chrom in enumerate(sorted(GENOME.keys())):

    print(elapsed(), '- Processing VCF:', chrom)
    sys.stdout.flush()

    vcf_dir = '{}/{}-simulated_snps.vcf.gz'.format(args.vcfd, chrom)
    seq = GENOME[chrom]
    prev_pos = 0
    for line in gzip.open(vcf_dir, 'rt'):

        # extract vcf header
        msp = []
        iden = []
        if line[0] == '#':
            if line[1:6] == 'CHROM':
                if i == 0:
                    fields = line.strip('\n').split('\t')
                    for f in fields:
                        if f[0:3] == 'msp':
                            msp.append(f)
                        else:
                            iden.append(f)
                    # determine pad size based on the length of largest sample number        
                    pad = '0{}d'.format(len(msp[-1][4:]))
                    for m in range(len(msp)):
                        iden.append('msp_' + str(format(m, pad)))
                    out.write('\t'.join(iden)+'\n')
            continue

        fields = line.strip('\n').split('\t')

        pos = int(fields[1]) - 1 # 0-based position of the SNP in the reference sequence

        # in case any simulated positions are larger than the reference chromosome...
        if pos >= len(seq):
            break

        fields[0] = chrom # chromosome ID

        # remove variants if they are overlapping with indel sizes
        if pos <= prev_pos:
            continue

        # determine the new sequences of the reference and alternative alleles
        fields[3], fields[4] = change_nuc(seq, pos, args.indp)

        # If we are dealing with an indel, increase the prev_pos value.
        if len(fields[3]) > 1 or len(fields[4]) > 1:
            prev_pos = pos + len(fields[3])

        # write new VCF line
        out.write('\t'.join(fields)+'\n')

#
# close and finish
#
out.close()
print( elapsed(), '- Total Time')
sys.stdout.flush()

