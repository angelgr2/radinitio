#!/usr/bin/env python3

import msprime
import time, sys, optparse, gzip
import numpy as np
#
# set command line variables:
#
p = optparse.OptionParser()
p.add_option('-c', '--chrom-id',  type='str',   dest='chrom',     help='chromosome name/id')
p.add_option('-l', '--length',    type='int',   dest='len',       help='length of chromosome in bp')
p.add_option('-o', '--outpath',   type='str',   dest='out',       help='output directory')
p.add_option(      '--n-pops',    type='int',   dest='npops',     help='number of populations')
p.add_option(      '--eff-sizes', type='str',   dest='ne',        help='Integer (or comma-sep list of integers) giving populations Ne')
p.add_option(      '--n-samples', type='str',   dest='n_samples', help='Integer (or comma-sep list of integers) giving the number of samples per population')
p.add_option(      '--recomb-r',  type='float', dest='recom_r',   help='recombination rate per base per generation')
p.add_option(      '--mut-r',     type='float', dest='mut_r',     help='mutation rate per base per generation')
p.add_option(      '--mig-r',     type='float', dest='mig_r',     help='symmetric migration rate, ~4m (m = per generation migration rate).')

opts, args = p.parse_args()

ne_list = [int(float(ne)) for ne in opts.ne.split(',')]
if len(ne_list) == 1:
    ne_list = [ne_list[0] for i in range(opts.npops)]
elif len(ne_list) != opts.npops:
    print('Error: bad --eff-size parameter')
    sys.exit()

samples_list = [int(float(s)) for s in opts.n_samples.split(',')]
if len(samples_list) == 1:
    samples_list = [samples_list[0] for i in range(opts.npops)]
elif len(samples_list) != opts.npops:
    print('Error: bad --n-samples parameter')
    sys.exit()

starttime = time.time()
def elapsed():
    return round(time.time()-starttime, 2)

#
# create msprime objects for population information
#
pop_conf = []
for i in range(opts.npops):
    x = msprime.PopulationConfiguration(
        sample_size=(samples_list[i] * 2),
        initial_size=ne_list[i],
        growth_rate=0.0)
    pop_conf.append(x)


size = 0
for n in ne_list:
    size += n

#
# determine migration parameters
#
if opts.npops > 1:
    m = opts.mig_r / (4 * (opts.npops - 1)) # per generation migration rate
matrix = [ [ 0 if i == j else m
         for j in range(opts.npops) ]
         for i in range(opts.npops) ]

#
# create msprime simulation object
#
sims = msprime.simulate(
    Ne=size,
    length=opts.len,
    recombination_rate=opts.recom_r,
    mutation_rate=opts.mut_r,
    population_configurations=(pop_conf),
    migration_matrix=matrix)

#
# output simulation as a VCF
#
out_vcf = opts.out+'/'+opts.chrom+'-simulated_snps.vcf.gz'
with gzip.open(out_vcf, 'wt') as vcf_file:
    sims.write_vcf( vcf_file, 2 )

#
# output and finish
#
print( elapsed(), '- Total Time:', opts.chrom)
sys.stdout.flush()
