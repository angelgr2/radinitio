#!/usr/bin/env python3

import sys, re, gzip, time, argparse, os
program_name = os.path.basename(__file__)

#
# set command line variables:
#
p = argparse.ArgumentParser()
p.add_argument('-g', '--genome',     dest='geno')
p.add_argument('-v', '--master-vcf', dest='vcfd')
p.add_argument('-o', '--outdir',    dest='outd')
p.add_argument('-r', '--reference',  dest='ref')
p.add_argument('-e', '--enz',        dest='enz')

args = p.parse_args()

# Overwrite the help/usage behavior.
p.format_usage = lambda : '''\
{prog} --genome path --master-vcf path --outdir dir --reference path --enz str

Input/Output files:
    -g, --genome: path to reference genome (gzipped fasta file)
    -v, --master-vcf: path to master VCF with snps for all chromosomes (gzipped VCF)
    -o, --outdir: path to an output directory
    -r, --reference: path and basename for RAD reference files (fasta, VCF, distributions)

Library preparation/sequencing:
    -e, --enz: restriction enzyme (sbfI, pstI, ecoRI, bamHI, etc.)

'''.format(prog=program_name)
p.format_help = p.format_usage

for required in ['geno', 'vcfd', 'outd', 'ref', 'enz']:
    if args.__dict__[required] is None:
        print('Required argument \'{}\' is missing.\n'.format(required), file=sys.stderr)
        p.print_help()
        sys.exit(1)

if args.outd[-1] != '/':
    args.outd += '/'

# Print usae information
print(' '.join(sys.argv))

#
# set the restriction enzyme class and related variables
#
class REnz:
    def __init__(self, enzyme_name, enzyme_cutsite, enzyme_remainder):
        self.name = enzyme_name
        self.cutsite = enzyme_cutsite
        self.remainder = enzyme_remainder

    def olap_len(self):
        return 2 * len(self.remainder) - len(self.cutsite)

# Contains sequences and remainders for all known enzymes
    # [ (enzyme_name, enzyme_cutsite, enzyme_remainder), ... ]
known_enzymes = [
    REnz('sbfI',  'CCTGCAGG', 'TGCAGG'),
    REnz('pstI',  'CTGCAG',   'TGCAG'),
    REnz('ecoRI', 'GAATTC',   'AATTC'),
    REnz('bamHI', 'GGATCC' ,  'GATCC')]

# Convert to dictionary
    # { enzyme_name : [enzyme_cutsite, enzyme_remainder], ... }
known_enzymes = dict([ (e.name, e) for e in known_enzymes ])

# Set restriction enzyme variables
    # enz.name = enzyme_name
    # enz.cutsite = enzyme_cutsite
    # enz.remainder = enzyme_remainder
enz = known_enzymes[args.enz]

pattern = '{}|{}'.format(
    enz.cutsite[ : (len(enz.cutsite) - len(enz.remainder)) ],
    enz.cutsite[ (len(enz.cutsite) - len(enz.remainder)) : ])
print('\nExtracting loci using restriction enzyme:\n {} ({})'.format(enz.name, pattern))

#
# Set the Variant class and related variables
#
class Variant:
    def __init__(self, position_bp, reference_allele, alternative_allele, genotypes):
        self.pos  = position_bp
        self.ref  = reference_allele
        self.alt  = alternative_allele
        self.geno = genotypes
        # Structure: (50125, A, T, 1101100010)

#
# function to reverse complete sequence. It finds complement and inverts the order
#
def rev_comp(seq):
    rev = []
    for nt in seq:
        if nt == 'A':
            rev.append('T')
        elif nt == 'C':
            rev.append('G')
        elif nt == 'G':
            rev.append('C')
        elif nt == 'T':
            rev.append('A')
        elif nt not in ['A', 'C', 'G', 'T']:
            rev.append('N')
    return ''.join(rev[::-1])

starttime = time.time()
def elapsed():
    return '{:.2f}'.format(time.time()-starttime)

#
# function to generate genome dictionary
# structure of this dictionary:     { chromosome_id : ATCGCAGGACTTACG... }
#
def genome_dict(genome_path):
    genome_d = {}
    seq = []
    head = None
    for line in gzip.open(genome_path, 'rt'):
        row = line.strip('\n')
        if row[0] == '>':
            if head != None:
                if len(''.join(seq)) >= 5000:   
                    # Don't add sequences if they're smaller than 5Kb
                    genome_d[head] = ''.join(seq)
            regex = re.match(r'>(\S+)', line)
            #this creates a key for the dict using just the group number and not the whole fasta header
            head = regex.group(1)
            seq = []
        elif row[0] != '>':
            seq.append(row.upper())
    if len(''.join(seq)) >= 5000:
        genome_d[head] = ''.join(seq)
    del seq
    return genome_d


#
# load the genome into GENOME dictionary
# will contain sequences for all chromosomes/scaffolds
# structure of this dictionary:     { chromosome_id : ATCGCAGGACTTACG... }
#
print(elapsed(), '- Loading the genome...')
sys.stdout.flush()

# genome dictionary
GENOME = genome_dict(args.geno)

# chromosome list
# For all of subsequent dictionaries, the keys are the chromome IDs (ex: groupI, groupII, etc) coming from CHROMS
CHROMS = sorted(GENOME.keys())

vout = gzip.open((args.ref+'.vcf.gz'), 'wt')
fa_f = gzip.open((args.ref+'.fa.gz'), 'wt')
dist = gzip.open((args.ref+'.dist.gz'), 'wt')

#
# find rad loci per chromosome - into RAW_CUTS dictionary
# will contain the positions of all the cut sites in all chromosomes - from the finditer search of enz.cutsite
# { groupI : [ pos1, pos2, pos3, ... ] }
#
print(elapsed(), '- Finding cutsites...')
sys.stdout.flush()
# Populate the dictionary using the chromosome keys form CHROM
RAW_CUTS = { chrom : [] for chrom in CHROMS }

for chrom in CHROMS:
    print(elapsed(), '- For chromosome', chrom)
    for match in re.finditer(enz.cutsite, GENOME[chrom]):
        RAW_CUTS[chrom].append(match.start())

#
# extract SNPs from VCF into a list of SNPs per chromosome in SNPS dictionary
# will contain the SNPs of all chromosomes as (pos, A/T, genotypes) - from the simulated VCF
# { groupI : [(pos1, A, T, gt1gt2gt3gt4), ... ]}
#
print(elapsed(), '- Loading the SNPs...')
sys.stdout.flush()

# Populate the dictionary using the chromosome keys form CHROMS
SNPS = { chrom : [] for chrom in CHROMS }
samples = []    # will contain all the samples, as provided by the VCF; [ sam1, sam2, sam3, ... ]
curr_chrom = None
chrom_snps = None
chrom_cuts = None
curr_cut_i = None
inexistent_cuts = { chrom : set() for chrom in CHROMS }
for line in gzip.open(args.vcfd, 'rt'):
    if line[0] == '#':
        # Extract samples and output header to vcf
        if line[1:6] == 'CHROM':
            fields = line.strip('\n').split()
            header = []

            for f in fields:
                header.append(f)
                if f[0:3] == 'msp':
                    samples.append(f)
            vout.write('\t'.join(header)+'\n')

        # output master VCF header to filtered VCF (except contig IDs)
        elif line[2:8] != 'contig':
            vout.write( line )

        continue

    t = line.index('\t')

    # Look at the chromosome.
    if line[:t] != curr_chrom:
        curr_chrom = line[:t]
        chrom_snps = []
        SNPS[curr_chrom] = chrom_snps
        chrom_cuts = RAW_CUTS[curr_chrom]
        curr_cut_i = 0

    # Parse the position.
    pos = int(line[t+1:line.index('\t', t+1)]) - 1

    # Only process the SNP if it is within 1000bp of a cutsite.
    while curr_cut_i != len(chrom_cuts) and pos > chrom_cuts[curr_cut_i] + (1000 + (len(enz.cutsite) - len(enz.remainder)) ):
        curr_cut_i += 1
    if curr_cut_i == len(chrom_cuts):
        # All remaining records for this chromosome are past the last locus.
        continue
    if pos < chrom_cuts[curr_cut_i] - (1000 - len(enz.remainder) ):
        # The next locus is far after the record.
        continue

    # Process the record.
    fields = line.strip('\n').split('\t')
    genotypes = []
    for i in range(9, len(fields)):
        genotypes.append(fields[i][0])
        genotypes.append(fields[i][2])
    chrom_snps.append((
            pos,
        fields[3],
        fields[4],
        ''.join(genotypes)
        ))
        # [ (pos1, A, T, gt1gt2gt3gt4), ... ]

    # Define cutsite boundary
    cutsite_first = chrom_cuts[curr_cut_i]
    cutsite_last  = cutsite_first + len(enz.cutsite) - 1

    # Find variants that modify the cutsite, if any.
    if pos >= cutsite_first and pos < cutsite_last + 1:
        if ''.join(genotypes) == '1' * (len(samples) * 2):
            # The cutsite doesn't exist in any of our individuals.
            inexistent_cuts[chrom].add(cutsite_first)

#
# Filter Cutsites. Removal of:
#	Less than 1000bp into chromose or within last 1000bp
#	Overlapping Loci
#	Cutsites with fixed mutations

# New Filtered Cuts dictionary:
print(elapsed(), '- Filtering cutsites and generating distribution...', flush=True)
dist.write('#chrom_id\tcut_pos_start\tcut_pos_end\tcutsite_status\n')
CUTS = { chrom : [] for chrom in CHROMS }
for chrom in CHROMS:
    chrom_len = len(GENOME[chrom])
    raw_cuts = RAW_CUTS[chrom]
    for i, cut in enumerate(raw_cuts):
        # Define locus boundary variables
        cutsite_first = cut
        cutsite_last = cutsite_first + len(enz.cutsite) - 1
        forward_first = cutsite_first + (len(enz.cutsite) - len(enz.remainder))
        forward_last = forward_first + 1000 - 1
        reverse_last = cutsite_first + len(enz.remainder) - 1
        reverse_first = reverse_last - 1000 + 1

        if reverse_last + 1 <= 1000 or forward_last + 1 >= chrom_len:
            # too close to chromosome end
            status = 'rm_chrom_end'
        elif i > 0 and cutsite_first - raw_cuts[i-1] < 2 * 1000 - enz.olap_len():
            # too close to another cutsite
            status = 'rm_too_close'
        elif i < len(raw_cuts) - 1 and raw_cuts[i+1] - cutsite_first < 2 * 1000 - enz.olap_len():
            # too close to another cutsite
            status = 'rm_too_close'
        elif cut in inexistent_cuts[chrom]:
            # this cutsite does not actually exist
            status = 'rm_fixed_absence'
        else:
            CUTS[chrom].append(cut)
            status = 'kept'

        # Print cutsite info to distributions file
        dist.write('{}\t{}\t{}\t{}\n'.format(
            chrom,
            cutsite_first,
            cutsite_last,
            status))

del RAW_CUTS

#
# extract rad loci
#
print(elapsed(), '- Extracting RAD loci...', flush=True)
cut_i = 1000 - len(enz.remainder)
for sam_i, sam in enumerate(samples):
    print(elapsed(), '- Extracting Sample:', sam_i, flush=True)

    outpath = args.outd+sam+'.fa.gz'
    out = gzip.open(outpath, 'wt')

    loc_num = 0                       # Loci number (ID) continuous across all chromosomes
    for chrom in CHROMS:
        curr_snps = []                # List of current snps (in loci)
        csnps_index = 0               # Index of the current SNP in the list
        cuts = CUTS[chrom]            # Contains the list of cut sites for that chromosome
        seq = GENOME[chrom]           # Contains sequence for that chromosome
        variants = SNPS[chrom]        # Contains the list of variants for that chromosome

        for cuts_i, cut in enumerate(cuts):
            #
            # Define locus boundary variables
            #
            cutsite_first = cut
            cutsite_last  = cutsite_first + len(enz.cutsite) - 1
            forward_first = cutsite_first + (len(enz.cutsite) - len(enz.remainder))
            forward_last  = forward_first + 1000 - 1
            reverse_last  = cutsite_first + len(enz.remainder) - 1
            reverse_first = reverse_last - 1000 + 1

            # skip loci if the reference contains Ns
            if 'N' in seq[ reverse_first : forward_last + 1 ]:
                continue

            # remove SNPs from previous loci from list
            while len(curr_snps) > 0 and curr_snps[0][0] < reverse_first:
                curr_snps = curr_snps[1:]

            # go over new snps
            while csnps_index < len(variants) and variants[csnps_index][0] < reverse_first:
                csnps_index += 1
            while csnps_index < len(variants) and variants[csnps_index][0] < forward_last + 1:
                curr_snps.append(variants[csnps_index])
                csnps_index += 1

            #
            # If this is the first sample, write to the RAD-seq FASTA & VCF files.
            #
            if sam_i == 0:

                # Write the FASTA file.
                fa_f.write('>t{:05d}n ref_pos={}:{}-{}\n{}\n'.format(
                    loc_num,
                    chrom,
                    reverse_first + 1,
                    reverse_last + 1,
                    rev_comp(seq[ reverse_first : reverse_last + 1 ])
                    ))

                fa_f.write('>t{:05d}p ref_pos={}:{}-{}\n{}\n'.format(
                    loc_num,
                    chrom,
                    forward_first + 1,
                    forward_last + 1,
                    seq[ forward_first : forward_last + 1 ]
                    ))

                # Write the VCF file.
                # VCF format is:
                # CHROM  POS  ID  REF  ALT  QUAL  FILTER  INFO  FORMAT  sam_0  sam_1  sam_2  ...

                # Find variants that modify the cutsite, if any.
                cutsite_gts = None
                cutsite_variants = []
                for snp in curr_snps:
                    if snp[0] >= cutsite_first and snp[0] < cutsite_last + 1:
                        cutsite_variants.append(snp)
                    elif snp[0] < cutsite_first and snp[0] + len(snp[1]) > cutsite_first:
                        # deletion upstream of the cutsite
                        cutsite_variants.append(snp)
                if cutsite_variants:
                    cutsite_gts = ['0' for i in range(len(cutsite_variants[0][3]))]
                    for v in cutsite_variants:
                        for i, allele in enumerate(v[3]):
                            if allele != '0':
                                assert allele == '1'
                                cutsite_gts[i] = '1'
                    cutsite_gts = ''.join(cutsite_gts)

                # Negative-strand locus.
                if cutsite_gts:
                    # Write a <DEL> variant.
                    vout.write('t{loc:05d}n\t1\t.\t{ref}\t<DEL>\t.\tPASS\tref_pos={chr}:{pos}\tGT'.format(
                        loc = loc_num,
                        ref = seq[reverse_last],
                        chr = chrom,
                        pos = reverse_last + 1    # (This is actually the position of the locus.)
                        ))
                    for i, gt in enumerate(cutsite_gts):
                        if i % 2 == 0:
                            vout.write('\t{}|{}'.format(gt, cutsite_gts[i+1]))
                    vout.write('\n')

                for snp in curr_snps[::-1]:
                    if snp[0] >= cutsite_first or snp[0] + len(snp[1]) - 1 >= cutsite_first:
                        # This variant is in the positive locus or affects the cutsite; skip it.
                        continue

                    # Reverse complement ref & alt.
                    pos = snp[0]
                    ref = snp[1]
                    alt = snp[2]
                    if len(ref) > 1:
                        # deletion
                        assert len(alt) == 1
                        pos += len(ref)
                        ref = ref[1:] + seq[snp[0] + len(ref)]
                    elif len(alt) > 1:
                        # insertion
                        pos += 1
                        alt = alt[1:] + seq[snp[0] + 1]
                    ref = rev_comp(ref)
                    alt = rev_comp(alt)

                    # Add the 'downstream of deletion' i.e. 'dropout' allele (if the cutsite varies).
                    if cutsite_gts:
                        alt += ',*'

                    vout.write('t{loc:05d}n\t{col}\t.\t{ref}\t{alt}\t.\tPASS\tref_pos={chr}:{pos}\tGT'.format(
                        loc = loc_num,
                        col = reverse_last - pos + 1,
                        ref = ref,
                        alt = alt,
                        chr = chrom,
                        pos = pos + 1
                        ))
                    for i, gt in enumerate(snp[3]):
                        if i % 2 == 0:
                            if not cutsite_gts or cutsite_gts[i:i+2] == '00':
                                gt = '\t{}|{}'.format(gt,snp[3][i+1]) # N.B. len(snp[3]) should be even.
                            elif cutsite_gts[i:i+2] == '01':
                                gt = '\t{}|2'.format(gt)
                            elif cutsite_gts[i:i+2] == '10':
                                gt = '\t2|{}'.format(snp[3][i+1])
                            else:
                                assert cutsite_gts[i:i+2] == '11'
                                gt = '\t2|2'
                            vout.write(gt)
                    vout.write('\n')

                # Positive-strand locus.
                # Same as for the negative-strand locus, except the offsets are simpler.
                if cutsite_gts:
                    vout.write('t{loc:05d}p\t1\t.\t{ref}\t<DEL>\t.\tPASS\tref_pos={chr}:{pos}\tGT'.format(
                        loc = loc_num,
                        ref = seq[forward_first],
                        chr = chrom,
                        pos = forward_first + 1
                        ))
                    for i, gt in enumerate(cutsite_gts):
                        if i % 2 == 0:
                            vout.write('\t{}|{}'.format(gt, cutsite_gts[i+1]))
                    vout.write('\n')

                for snp in curr_snps:
                    if snp[0] < cutsite_last + 1:
                        continue

                    vout.write('t{loc:05d}p\t{col}\t.\t{ref}\t{alt}\t.\tPASS\tref_pos={chr}:{pos}\tGT'.format(
                        loc = loc_num,
                        col = snp[0] - forward_first + 1,
                        ref = snp[1],
                        alt = (snp[2] + ',*') if cutsite_gts else snp[2],
                        chr = chrom,
                        pos = snp[0] + 1
                        ))
                    for i, gt in enumerate(snp[3]):
                        if i % 2 == 0:
                            if not cutsite_gts or cutsite_gts[i:i+2] == '00':
                                gt = '\t{}|{}'.format(gt,snp[3][i+1]) # N.B. len(snp[3]) should be even.
                            elif cutsite_gts[i:i+2] == '01':
                                gt = '\t{}|2'.format(gt)
                            elif cutsite_gts[i:i+2] == '10':
                                gt = '\t2|{}'.format(snp[3][i+1])
                            else:
                                assert cutsite_gts[i:i+2] == '11'
                                gt = '\t2|2'
                            vout.write(gt)
                    vout.write('\n')

            #
            # Write both alleles.
            #
            for allele in [0, 1]:

                # create copy of the locus
                copy = list(seq[ reverse_first : forward_last + 1 ])
                cut_shift = 0             # cutsite position shift after indels

                ref_l = len(copy)         # length of reference sequence (for CIGAR purposes)
                mat_l = 0                 # last match position (for CIGAR purposes)
                cigar = []                # list containing the CIGAR operations

                for s_i, snp in enumerate(curr_snps):
                    geno = int(snp[3][ (sam_i * 2) + allele ])
                    pos = snp[0] - reverse_first
                    ref = snp[1]
                    alt = snp[2]
                    if geno == 1:

                        mat_s = pos + 1  # start match position (for CIGAR purposes)

                        if len(ref) == 1 and len(alt) == 1:
                            # substitution
                            copy[pos] = alt

                        elif len(ref) > 1:
                            # deletion
                            if pos < cut_i:
                                cut_shift -= len(ref) - 1
                            for r in range(len(ref) - 1):
                                d_i = pos + r + 1
                                if d_i < ref_l:
                                    copy[d_i] = ''

                            # For CIGAR
                            ind_l = len(ref) - 1
                            cigar.append('{:d}M'.format(mat_s - mat_l))
                            cigar.append('{:d}D'.format(ind_l))
                            mat_l = mat_s + ind_l

                        elif len(alt) > 1:
                            # insertion
                            copy[pos] = alt
                            if pos < cut_i:
                                cut_shift += len(alt) - 1

                            # For CIGAR
                            ind_l = len(alt) - 1
                            cigar.append('{:d}M'.format(mat_s - mat_l))
                            cigar.append('{:d}I'.format(ind_l))
                            mat_l = mat_s

                # fill the rest of the CIGAR
                if mat_l != ref_l:
                    cigar.append('{:d}M'.format(ref_l - mat_l))

                # remove allele if it has mutations in the cut site
                new = ''.join(copy)
                seq_cutsite = new[ cut_i + cut_shift : cut_i + cut_shift + len(enz.cutsite) ]
                if seq_cutsite != enz.cutsite:
                    continue

                # check CIGAR operations at the cutsite and create CIGAR for forward and reverse locus
                cig_f = []                      # cigar for forward locus
                cig_r = []                      # cigar for reverse locus
                cnt = 0
                split = False                   # check if cigar has been split
                discard = False                 # check if locus needs to be discarded
                for cig in cigar:
                    size = int(cig[:-1])
                    cig_operation = cig[-1]
                    # do not count size of indels
                    if cig_operation != 'I':
                        cnt += size
                    # if before the split (cutsite)
                    if cnt <= cut_i:
                        cig_r.append(cig)
                    # if after the split (cutsite)
                    elif cnt > cut_i:
                        # check the integrity of the cutsite
                        if split == False:
                            if cnt < (cut_i + 1 + 7):
                                # if the operation does not cover the whole cutsite
                                discard = True
                                break

                            if cig_operation != 'M':
                                # if the operation is not a match
                                discard = True
                                break

                            # if cutsite is intact, split CIGAR
                            cig_f.append('{:d}{}'.format(
                                ((cnt - cut_i) - (len(enz.cutsite) - len(enz.remainder)) ),
                                cig_operation))
                            cig_r.append('{:d}{}'.format(
                                (size - (cnt - cut_i) + len(enz.remainder) ),
                                cig_operation))
                            split = True
                        # add the other matches into the forward cigar
                        elif split == True:
                            cig_f.append(cig)

                if discard == True:
                    continue

                # split into forward and reverse locus
                f_locus = new[ cut_i + cut_shift + (len(enz.cutsite) - len(enz.remainder)) : ]
                r_locus = rev_comp(new[ : cut_i + cut_shift + len(enz.remainder) ])

                # print RAD loci with changed SNPs - forward locus
                out.write('>t{:05d}p:{}:a{:d} cig={}\n'.format(loc_num, sam, (allele + 1), ''.join(cig_f)))
                out.write('{}\n'.format(f_locus))

                # print RAD loci with changed SNPs - reverse locus
                out.write('>t{:05d}n:{}:a{:d} cig={}\n'.format(loc_num, sam, (allele + 1), ''.join(cig_r[::-1])))
                out.write('{}\n'.format(r_locus))

            loc_num += 1

    out.close()

vout.close()

print(elapsed(), '- Total Time')
