#! /usr/bin/env python3

import random, re, gzip, time, sys, optparse, os, multiprocessing
import numpy as np

#
# reverse complement sequences
#
def rev_comp(seq):
    rev = []
    for nt in seq:
        if nt == 'A':
            rev.append('T')
        elif nt == 'C':
            rev.append('G')
        elif nt == 'G':
            rev.append('C')
        elif nt == 'T':
            rev.append('A')
        else:
            rev.append('N')
    return ''.join(rev[::-1])

#
# split header CIGAR strings
#
def split_cigar (cigar):
    splitcig = []
    prev = 0
    for i, c in enumerate(cigar):
        if c in ('M', 'I', 'D'):
            splitcig.append(cigar[prev:i+1])
            prev=i+1
    return splitcig

#
# Extracts a read CIGAR from its locus CIGAR.
# Input cigar should be like `['4M', '1D', '7M']`
# Takes input from split_cigar()
#
def extract_cigar(cig, start, read_len):
    cig_n = []
    ref_consumed = 0
    read_consumed = 0

    for op in cig:
        type = op[-1]
        if not type in ('M','D','I'):
            raise Exception('MDI')
        size = int(op[:-1])

        # Fast forward to the read region.
        if len(cig_n) == 0:
            if type == 'M':
                if ref_consumed + size <= start:
                    ref_consumed += size
                    continue
                elif ref_consumed < start:
                    consume = start - ref_consumed
                    ref_consumed += consume
                    size -= consume
            if type == 'D':
                start += size
                ref_consumed += size
                continue
            elif type == 'I':
                start -= size
                if ref_consumed <= start:
                    continue
                else:
                    size = ref_consumed - start

            if ref_consumed > 0:
                cig_n.append('{}H'.format(ref_consumed))

        # Write the cigar operations within the reads.
        if type == 'M':
            consume = min(size, read_len - read_consumed)
            cig_n.append('{}M'.format(consume))
            read_consumed += consume
            ref_consumed += consume
        elif type == 'D':
            cig_n.append(op)
            ref_consumed += size
        elif type == 'I':
            consume = min(size, read_len - read_consumed)
            cig_n.append('{}I'.format(consume))
            read_consumed += consume

        # Break after the read.
        if read_consumed == read_len:
            if ref_consumed < 1000:
                cig_n.append('{}H'.format(1000 - ref_consumed))
            break
        else:
            assert read_consumed < read_len

    return cig_n

#
# Function to add sequencing errors to a sequence.
# `err_probs` should be a `(big_int, [int_thresholds])`
#
def seq_error(seq, err_probs):
    assert len(err_probs[1]) == len(seq)
    lseq = list(seq)
    rdm = np.random.randint(err_probs[0], size=len(seq))
    for i in range(len(seq)):
        if rdm[i] < err_probs[1][i]:
            lseq[i] = random.choice(['A','C','G','T'])
    return ''.join(lseq)

#	
# Perform actual read processing
# add error, read length, PCR duplicates, etc
#
def simulate_read_pair(locus, read_len, err_probs, pcr_dup, reads1_f, reads2_f, i):

    # produce read name
    insert_len = locus[2]
    loc_fields = locus[0].split(' ')
    loc_id = loc_fields[0]
    assert loc_fields[1][:4] == 'cig='
    loc_cigar = split_cigar(loc_fields[1][4:])
    cig1 = extract_cigar(loc_cigar, 0, read_len)
    cig2 = extract_cigar(loc_cigar, (insert_len - read_len), read_len)[::-1]
    read_n = '{}:r{}:{}:cig1={}:cig2={}'.format(loc_id, i, insert_len, ''.join(cig1), ''.join(cig2))
    read_1 = seq_error(locus[1][:read_len], err_probs)
    read_2 = rev_comp(seq_error(locus[1][(insert_len - read_len):insert_len], err_probs))

    # Forward read.
    reads1_f.write('>{}/1\n'.format(read_n))
    reads1_f.write(read_1)
    reads1_f.write('\n')

    # Reverse read.
    reads2_f.write('>{}/2\n'.format(read_n))
    reads2_f.write(read_2)
    reads2_f.write('\n')
	
	# Add PCR duplicates. 
    duplicate = np.random.choice(['yes', 'no'], p = [pcr_dup, 1-pcr_dup])
    if duplicate == 'yes':

        # Determine number of copies.
        copies = 0
        while copies == 0:
            copies = np.random.poisson(lam=1)
		
        for c in range(copies):
            # Forward read duplicate.
            reads1_f.write('>{}:PCRdup/1\n'.format(read_n))
            reads1_f.write(read_1)
            reads1_f.write('\n')

            # Reverse read duplicate.
            reads2_f.write('>{}:PCRdup/2\n'.format(read_n))
            reads2_f.write(read_2)
            reads2_f.write('\n')
	
#
# restriction enzyme list
# structure is [enzyme seq, pos of for cut, pos of rev cut]
#
RENZ = {}                   
RENZ['mspI'] = ['CCGG', 1, 3]
RENZ['mseI'] = ['TTAA', 1, 3]

#	
# function to define actual dd_loci list (loci with both cutsites and correct size range)
# includes only loci that have the second enzyme cut within the desired size distribution
#
def determine_ddloci(loci_list, enzyme, insert_min, insert_max):
    
    # define enzyme parameters
    enz = RENZ[enzyme]

	# iterate over loci list
    dd_loci = []           # [ (header, seq, ins_len), ... ]
    for locus in loci_list:
        # determine position of enzyme cut in sequence (insert length)	    
        match = re.search(enz[0], locus[1])
        cut = None
        if match == None:
            cut = 0
        else:
            cut = match.span()[0]
        insert_len = cut + enz[2]
	
        # confirm the insert size is within the desired size distribution
        if insert_len >= insert_min and insert_len <= insert_max:
            
			# if good, then add to dd_loci list
            new_locus = locus[1][:(insert_len + 1)]        
            dd_loci.append( (locus[0], new_locus, insert_len) )

    return dd_loci	

#
# open output and process each sample
#
def process_sample(*args):
    if len(args) == 1:
        args = args[0]
    sample, opts, err_probs = args

    print('Sample \'{}\'...'.format(sample))
    sys.stdout.flush()

    # Open output files.
    reads1_f = gzip.open('{}/{}.1.fa.gz'.format(opts.out_d, sample), 'wt')
    reads2_f = gzip.open('{}/{}.2.fa.gz'.format(opts.out_d, sample), 'wt')

    # Read in the loci.
    loci = [] # [ (header, seq), ... ]
    header = None
    for line in gzip.open('{}/{}.fa.gz'.format(opts.loci, sample), 'rt'):
        if line[0] == '>':
            header = line[1:-1]
        else:
            loci.append( ( header, line[:-1] ) )
    
    # Determine the dd_loci (loci with both cutsites and correct size range	)
    dd_loci = determine_ddloci(loci, opts.enz, opts.ins_min, opts.ins_max)
	
    # calculate the number of iterations necessary for desired coverage using ONLY dd_loci
    n_reads = int( (len(dd_loci) / 2) * opts.cov )

    # iterate randomly over our list of loci - number of iterations is representative of average coverage
    for i in range(n_reads):

        # select random loci from the list
        locus = random.choice(dd_loci)
        simulate_read_pair(locus, opts.rlen, err_probs, opts.pcr_d, reads1_f, reads2_f, i)

#
# Main.
#
def main():

    # Parse command line options.
    p = optparse.OptionParser()
    p.add_option('-l', '--loci-dir',   type='str',   dest='loci')
    p.add_option('-o', '--out-dir',    type='str',   dest='out_d')
    p.add_option('-c', '--coverage',   type='int',   dest='cov')
    p.add_option('-r', '--read-len',   type='int',   dest='rlen')
    p.add_option('-e', '--enz',        type='str',   dest='enz',      help='restriction enzyme')
    p.add_option('-m', '--ins-min',    type='int',   dest='ins_min',  help='minimum insert length')
    p.add_option('-x', '--ins-max',    type='int',   dest='ins_max',  help='maximum insert length')
    p.add_option('-i', '--init-error', type='float', dest='ierr',     help='sequencing error rate at the beginning (5\') of reads')
    p.add_option('-f', '--fin-error',  type='float', dest='ferr',     help='sequencing error rate at the end (3\') of reads')
    p.add_option('-p', '--pcr-dup-p',  type='float', dest='pcr_d',    help='PCR duplicate frequency')
    p.add_option('-t', '--n-threads',  type='int',   dest='nthreads', default=1)

    p.add_option('--outpath', type='str', dest='out_d') # (For backwards compatibility.)

    opts, args = p.parse_args()
    for required in ['loci', 'out_d', 'cov', 'rlen', 'enz', 'ins_min', 'ins_max', 'ierr', 'ferr', 'pcr_d']:
        if not opts.__dict__[required]:
            print('Required argument \'{}\' is missing.'.format(required), file=sys.stderr)
            p.print_help()
            sys.exit(1)

    opts.loci = opts.loci.rstrip('/')
    opts.out_d = opts.out_d.rstrip('/')
	
    if opts.ins_min <= opts.rlen:
        print('ERROR: Minimum insert length (-m) is smaller than read length (-r)!\n')
        p.print_help()
        sys.exit(1)
	
    # Report parameters
    print(' '.join(sys.argv),
        ' ',
        'Extracting RAD Reads with parameters:',
        '  Coverage: {}X'.format(opts.cov),
        '  Read Length: {}bp'.format(opts.rlen),
		'  Restriction Enzyme: {}'.format(opts.enz),
        '  Insert Size: min={}bp, max={}bp'.format(opts.ins_min, opts.ins_max),
        '  Sequencing Error: from {}% to {}%'.format(opts.ierr*100, opts.ferr*100),
        '  PCR Duplicate Frequency: {}%'.format(opts.pcr_d*100),
        sep='\n')

    # Find samples.
    samples = []
    for f in os.listdir(opts.loci):
        if f[:3] == 'msp':
            i = f.find('.')
            samples.append(f[:i])
    samples.sort()
    print('\nFound {} samples.'.format(len(samples)))
    assert len(samples) > 0

    # Create the array of sequencing error thresholds.
    big_int = 1e9
    err_probs = (
        big_int,
        np.array([
            int( (opts.ierr + (opts.ferr - opts.ierr) / (opts.rlen - 1) * i) * big_int) for i in range(opts.rlen)
            ], dtype=np.int64)
        )

    # Process all samples.
    print('\nSimulating reads...')
    multiprocessing.Pool(opts.nthreads).map(
        process_sample,
        [(sample, opts, err_probs) for sample in samples],
        chunksize=1
        )

    print('Done.')

if __name__ == '__main__':
    main()