#! /usr/bin/env python3

import random, gzip, time, math, sys, argparse, os, multiprocessing
import numpy as np
from scipy.stats import poisson, binom

program_name = os.path.basename(__file__)
starttime = time.time()
def elapsed():
    return '{:.2f}'.format(time.time()-starttime)

# Reverse compliment a sequence
def rev_comp(seq):
    rev = []
    for nt in seq:
        if nt == 'A':
            rev.append('T')
        elif nt == 'C':
            rev.append('G')
        elif nt == 'G':
            rev.append('C')
        elif nt == 'T':
            rev.append('A')
        else:
            rev.append('N')
    return ''.join(rev[::-1])

# Split CIGAR string into CIGAR list
def split_cigar (cigar):
    splitcig = []
    prev = 0
    for i, c in enumerate(cigar):
        if c in ('M', 'I', 'D'):
            splitcig.append(cigar[prev:i+1])
            prev=i+1
    return splitcig

#
# Extracts a read CIGAR from its locus CIGAR.
#
def extract_cigar(cigar, start, read_len):
    # Convert CIGAR from CIGAR string to CIGAR list
    cig_l = split_cigar(cigar)
    cig_n = []
    ref_consumed = 0
    read_consumed = 0

    for op in cig_l:
        type = op[-1]
        if not type in ('M','D','I'):
            raise Exception('MDI')
        size = int(op[:-1])

        # Fast forward to the read region.
        if len(cig_n) == 0:
            if type == 'M':
                if ref_consumed + size <= start:
                    ref_consumed += size
                    continue
                elif ref_consumed < start:
                    consume = start - ref_consumed
                    ref_consumed += consume
                    size -= consume
            if type == 'D':
                start += size
                ref_consumed += size
                continue
            elif type == 'I':
                start -= size
                if ref_consumed <= start:
                    continue
                else:
                    size = ref_consumed - start

            if ref_consumed > 0:
                cig_n.append('{}H'.format(ref_consumed))

        # Write the cigar operations within the reads.
        if type == 'M':
            consume = min(size, read_len - read_consumed)
            cig_n.append('{}M'.format(consume))
            read_consumed += consume
            ref_consumed += consume
        elif type == 'D':
            cig_n.append(op)
            ref_consumed += size
        elif type == 'I':
            consume = min(size, read_len - read_consumed)
            cig_n.append('{}I'.format(consume))
            read_consumed += consume

        # Break after the read.
        if read_consumed == read_len:
            if ref_consumed < 1000:
                cig_n.append('{}H'.format(1000 - ref_consumed))
            break
        else:
            assert read_consumed < read_len

    return cig_n

# Dictionary with mutation combinations
mutations_comb = {
    'A' : ['C','G','T'],
    'C' : ['A','G','T'],
    'G' : ['A','C','T'],
    'T' : ['A','C','G']}

#
# Function to add sequencing errors to a sequence.
# `err_probs` should be a `(big_int, [int_thresholds])`
#
def seq_error(seq, err_probs):
    assert len(err_probs[1]) == len(seq)
    lseq = list(seq)
    rdm = np.random.randint(err_probs[0], size=len(seq))
    for i in range(len(seq)):
        if rdm[i] < err_probs[1][i]:
            lseq[i] = mutations_comb[lseq[i]][np.random.randint(3)]
    return ''.join(lseq)

# sample and process a template molecule from all available alleles
def sample_a_template(alleles, ins_mean, ins_stdev, read_len):
    allele = random.choice(alleles)
    insert_len = 0
    while not read_len <= insert_len <= len(allele[1]):
        insert_len = int(np.random.normal(ins_mean, ins_stdev))
    return allele, insert_len

# generate new name for a sampled template
def build_base_template_name(allele, insert_len, read_len, clone_i):
    # produce read name
    fields = allele[0].split(' ')
    assert fields[1].startswith('cig=')
    cigar = fields[1][len('cig='):]
    fw_read_cig = ''.join( extract_cigar(cigar, 0, read_len) )
    rev_read_cig = ''.join( extract_cigar(cigar, (insert_len - read_len), read_len)[::-1] )
    name = '{}:cig1={}:cig2={}:{}'.format(fields[0], fw_read_cig, rev_read_cig, clone_i + 1)
    return name

# function to generate template molecule (sheared allele sequence)
# Simulate sequencing process for a given read pair
def sequence_read_pair(allele, insert_len, read_len, err_probs):
    fw_read = seq_error(allele[1][:read_len], err_probs)
    rev_read = rev_comp(seq_error(allele[1][(insert_len - read_len):insert_len], err_probs))
    return fw_read, rev_read

# Determine number of desired reads to obtain a coverage
def target_reads(n_alleles, coverage):
    n_reads = (n_alleles // 2) * coverage
    return int(n_reads)

# Determine number of per-clone PCR duplicates (size of clone)
def simulate_pcr_inherited_efficiency(mu, sigma, n_cycles):
    duplicates = 1
    #p = 0.5       # Probability of duplicating a read
    p = 0.0
    while not 0.0 < p <= 1.0:
        p = np.random.normal(mu, sigma)
    for i in range(n_cycles):
        duplicates += np.random.binomial(duplicates, p)
    return duplicates

# Overall distribution of PCR duplicates
#  This will determine clone size and frequency (probability)
#  Structure: Cpn, where p is the frequency of a clone of size n
#     [ Cp0, Cp1, Cp2, Cp3, Cp4, ... ]
def get_library_clone_size_distribution(pcr_model, n_sims=1e5):
    clone_histogram = {}
    for i in range(int(n_sims)):
        clone_size = pcr_model()
        clone_histogram.setdefault(clone_size, 0)
        clone_histogram[clone_size] += 1
    max_size = max(clone_histogram.keys())
    for i in range(max_size+1):
        clone_histogram.setdefault(i, 0)
    clone_histogram = [ clone_histogram[i] / n_sims for i in range(max_size+1) ]
    return clone_histogram

#
# Function to log convert the size clases of the amplified clone size distribution
# Converting the distribution will collapse size classes, decreasing the size of the distribution and improving efficiency
def log_convert_ampl_clone_dist(ampl_clone_size_distrib, base):
    # generate empy distribution of size x, where x is the log of the biggest clone in the original distribution
    a_max = len(ampl_clone_size_distrib) - 1
    log_ampl_clone_size_distrib = [ 0.0 for log_a in range(math.floor(math.log(a_max, base)) + 1) ]
    for a, prob in enumerate(ampl_clone_size_distrib):
        if a == 0:
            continue
        log_a = math.floor(math.log(a, base))
        log_ampl_clone_size_distrib[log_a] += prob
    assert 1.0-1e-9 < sum(log_ampl_clone_size_distrib) < 1.0+1e-9
    return log_ampl_clone_size_distrib

#
# Function to determine the clone sizes within a clone class
# log_a = is the log-transform of a clone size
def get_sizes_in_clone_class(log_a, base):
    assert log_a >= 0
    clone_class = range(
        math.ceil(base ** log_a),
        math.ceil(base ** (log_a + 1)) )
    if not clone_class:
        return None
    return clone_class

def get_clone_class_representative_value(log_a, base):
    if get_sizes_in_clone_class(log_a, base) is None:
        return None
    return round(base ** (log_a + 0.5))

#
# Calculate number of starting templates
#
def total_molecules_per_sample(insert_bp, dna_ng, n_samples, frac_rad_molecules=0.01):
    # The fraction of "good" RAD molecules when amplifying library.
    # Takes into account frequency of cuts, size selection, adapters, etc.
    avog = 6.022e23
    nuc_w = 660
    total_molecules = (( dna_ng * avog ) / ( insert_bp * 1e9 * nuc_w)) * frac_rad_molecules
    return int(total_molecules//n_samples)

#
# Total number of amplified molecules obtaining during the PCR amplification process
#
def get_total_amplified_molecules(ampl_clone_size_distrib, n_templates, base):
    total_amplified_molecules = 0.0
    for log_a, prob in enumerate(ampl_clone_size_distrib):
        a = get_clone_class_representative_value(log_a, base)
        if a is None:
            assert prob == 0.0
            continue
        total_amplified_molecules += a * prob
    total_amplified_molecules *= n_templates
    return int(total_amplified_molecules)

#
# Calculate number of mutated nodes in PCR clone tree
#
def mutated_node_freq(n_nodes):
    # 0 and 1 can never have mutations
    assert n_nodes >= 2
    # edges are the number of sucessful amplification reactions
    n_edges = n_nodes - 1
    # Mutations appear on a random edge
    edge_with_mut = np.random.randint(n_edges)
    # List storing the nodes count
    #   [no_mutations, mutations]
    nodes_cnt = [1 + edge_with_mut, 1]
    # Determine if later reactions stem from mutated node
    while (nodes_cnt[1] + nodes_cnt[0]) < n_nodes:
        # Prob of new mutated node
        p = nodes_cnt[1] / (nodes_cnt[1] + nodes_cnt[0])
        # Prob of new non-mutated node
        q = 1 - p
        # Select if new node is mutated (1) or not (0)
        i = np.random.choice([1, 0], p=[p, q])
        nodes_cnt[i] += 1
    # Return number of mutated nodes in the tree
    return nodes_cnt[1]

#
# Calculate the probability of obtaining no mutations in a given clone size
#
def prob_pcr_no_error(ampl_clone_size, read_len, pol_error):
    if ampl_clone_size <= 1:
        return 1.0
    else:
        p_no_err_read = (1 - pol_error) ** ((ampl_clone_size - 1) * (read_len * 2))
        return p_no_err_read

#
# Generate distribution of per clone probabilities of no error
#   Structure:  [ error_prob0, error_prob1, ... ]
#
def per_clone_no_error_prob_log_dist(read_len, log_ampl_clone_size_distrib, base, pol_error=4.4e-7):
    # pol_error default is the fidelity of Phusion HF Pol (per bp, per CPR cycle)
    per_clone_no_error = [ None for log_a in range(len(log_ampl_clone_size_distrib)) ]
    for log_a in range(len(log_ampl_clone_size_distrib)):
        a = get_clone_class_representative_value(log_a, base)
        if a is None:
            continue
        per_clone_no_error[log_a] = prob_pcr_no_error(a, read_len, pol_error)
    # Return list of error probabilities for the corresponding clone sizes classes
    return per_clone_no_error

#
# Per clone size, generate a distribution of the frequency of nodes with mutation
#   Structure: [ [ p0, p1, p2 ]
#                [ p0, p1, p2, p3 ],
#                ...,
#                [ p0, p1, p2, ..., pn ] ]
# First dimension of the list is the clone size classes, starting at class size 2 (0 & 1 have no error)
# Second dimension is distribution of errors in that clone
def generate_mut_node_log_distrib(log_ampl_clone_size_distrib, base, max_iter):
    # Empty list to hold the error distributions for all clone size classes
    mut_node_dist = [ None for log_a in range(len(log_ampl_clone_size_distrib)) ]
    # Iterate over the size classes
    for log_a in range(len(log_ampl_clone_size_distrib)):
        a = get_clone_class_representative_value(log_a, base)
        if a is None:
            continue
        if a < 2:
            continue
        # Iterate and keep tally of the mutated nodes generated
        clone_error_dist = [ 0 for n_mut in range(a + 1) ]
        for _ in range(max_iter):
            n_mut = mutated_node_freq(a)
            clone_error_dist[n_mut] += 1
        # Convert tally to frequency list
        mut_freq = [ clone_error_dist[n_mut]/max_iter for n_mut in range(a + 1) ]
        # Store in mut_node_dist list
        mut_node_dist[log_a] = mut_freq
    return mut_node_dist

#
# New distribution containing the adjusted mutated molecule distribution
# Merges `per_clone_no_error_prob_dist` and `generate_mut_node_distrib`
#   Structure: [ [ p0 ],
#                [ p0, p1 ], ...,
#                [ p0, p1, p2, ..., pn ] ]
# First dimension of the list is the clone size classes,
# Second dimension is distribution of errors in that clone
def adjust_dist_of_ampl_errors(log_ampl_clone_size_distrib, read_len, 
        base, max_iter=100, pol_error=4.4e-7):
    # Generate two base distributions
    clone_no_error_prob = per_clone_no_error_prob_log_dist(read_len, log_ampl_clone_size_distrib, base)
    mut_node_dist = generate_mut_node_log_distrib(log_ampl_clone_size_distrib, base, max_iter=100)
    assert len(clone_no_error_prob) == len(mut_node_dist)
    adj_error_dist = [ None for log_a in range(len(clone_no_error_prob)) ]
    for log_a, prob_no_mut in enumerate(clone_no_error_prob):
        if prob_no_mut is None:
            continue
        if log_a == 0:
            # a==1; no mutations.
            adj_error_dist[0] = [1.0, 0.0]
            continue
        assert mut_node_dist[log_a][0] == 0.0
        assert 1.0-1e-9 < sum(mut_node_dist[log_a]) < 1.0+1e-9
        adj_error_dist[log_a] = [ p_n_mut * (1 - prob_no_mut) for p_n_mut in mut_node_dist[log_a] ]
        adj_error_dist[log_a][0] = prob_no_mut
    return adj_error_dist

#
# Generate a single distribution containing both PCR error and duplicates by
# applying the following formula:
# p(S=s^ES=r) = sum_a{ p(A=a) p(S=s|A=a) sum_e{ p(EA=e|A=a) p(ES=r|S=s^A=a^EA=e) }}
def generate_pcr_dups_error_log_distrib(log_ampl_clone_size_distrib, n_sequenced_reads,
        total_amplified_molecules, base, single_adj_error_distrib=None):
    max_s = 1000    # Max size of sequenced clones
    p_lim = 1e-6    # Truncate distribution for values smaller than this
    seq_pcr_error_distrib = []
    #   Structure: [ [ p0 ],
    #                [ p0, p1 ], ...,
    #                [ p0, p1, p2, ..., pn ] ]
    # Half matrix containing probabilities of clone sizes and error
    # First dimension clone sizes
    # Second dimension errors in that clone size

    # Define the amplifed clone size classes
    a_classes = range(len(log_ampl_clone_size_distrib))
    # precompute the p(S=s|A=a)
    p_s_a = [None for log_a in a_classes]
    for log_a in a_classes:
        a = get_clone_class_representative_value(log_a, base)
        if a is None:
            continue
        p_s_a[log_a] = poisson.pmf(range(max_s), (n_sequenced_reads * a / total_amplified_molecules))
    p_s0 = None
    for s in range(max_s):
        seq_pcr_error_distrib.append([ None for r in range(s+1) ])
        # precompute the p(ES=r|S=s^A=a^EA=e)
        if single_adj_error_distrib is not None:
            p_r_sae = []
            for log_a in a_classes:
                p_r_sae.append(None)
                a = get_clone_class_representative_value(log_a, base)
                if a is None:
                    continue
                p_r_sae[-1] = [ binom.pmf(range(s+1), s, e/a) for e in range(a+1) ]
        for r in range(s+1):
            p_sr = 0.0
            for log_a in a_classes:
                a = get_clone_class_representative_value(log_a, base)
                if a is None:
                    continue
                if single_adj_error_distrib is None:
                    sum_e = 1.0 if r == 0 else 0.0
                else:
                    sum_e = 0.0
                    for e in range(a):
                        # sum_e{ p(EA=e|A=a) p(ES=r|S=s^A=a^EA=e)
                        p_e_a = single_adj_error_distrib[log_a][e]
                        sum_e += p_e_a * p_r_sae[log_a][e][r]
                # sum_a{ p(A=a) p(S=s|A=a) sum_e{ p(EA=e|A=a) p(ES=r|S=s^A=a^EA=e) }} for a given `a`
                p_sr += log_ampl_clone_size_distrib[log_a] * p_s_a[log_a][s] * sum_e
            if s == 0 :
                assert r == 0
                p_s0 = p_sr
                seq_pcr_error_distrib[0][0] = 0.0
            else:
                seq_pcr_error_distrib[s][r] = p_sr / (1 - p_s0)
        if s > 0 and sum(seq_pcr_error_distrib[s]) < p_lim:
            # break when values are getting too small
            break
    assert len(seq_pcr_error_distrib) > 1
    # Return double matrix
    return seq_pcr_error_distrib

#
# Convert the sequenced clone/error matrix into two lists (one of values and one of probabilities)
# Values are (x, y), where x is a sequenced clone size, y is number of molecules with error in that sequenced clone
def get_seq_pcr_error_lists(seq_pcr_error_distrib):
    seq_clone_errors_freq = []
    seq_clone_errors_vals = []
    for s, probs in enumerate(seq_pcr_error_distrib):
        for r, p in enumerate(probs):
            seq_clone_errors_freq.append(p)
            seq_clone_errors_vals.append((s, r))
    # Adjust values so sum of probabilities equals 1
    #   This is product of truncating the distribution for really small values
    sum_p = np.sum(seq_clone_errors_freq)
    if sum_p < 1:
        seq_clone_errors_freq = [ seq_clone_errors_freq[i]/sum_p for i in range(len(seq_clone_errors_freq)) ]
    # Return lists of clone values and adjusted frecuency
    return seq_clone_errors_freq, seq_clone_errors_vals

#
# Convert distribution of clone sizes AND errors into clone sizes only
def get_duplicate_only_distrib(seq_clone_errors_freq, seq_clone_errors_vals):
    dup_distrib = {}
    # Iterate over the clone+error distribution and sum all frequencies for a given clone size
    for i in range(len(seq_clone_errors_freq)):
        clone_size = seq_clone_errors_vals[i][0]
        dup_distrib.setdefault(clone_size, 0)
        dup_distrib[clone_size] += seq_clone_errors_freq[i]
    dup_distrib = [ dup_distrib[clone_size] for clone_size in sorted(dup_distrib.keys()) ]
    # Return a list containing the per-sequenced clone size frequencies
    return dup_distrib

#
# From the distribution of sequenced clones, calculate the percentage of PCR duplicates
def calculate_perc_duplicates(dup_distrib):
    # duplicates = to 1 - (1 / sum_reads)
    # sum_reads = 1*p(S=1) + 2*p(S=2) + 3*p(S=3) + ...
    # 1 - sum_reads is the portion of reads that are kept
    sum_reads = 0.0
    for clone_size, prob in enumerate(dup_distrib):
        if clone_size == 0:
            continue
        # Do n*p(S=n), where n is the clone size
        sum_reads += (clone_size * prob)
    # Return 1 - kept_reads
    return 1 - (1 / sum_reads)

#
# Sample from the merged sequencing clone size/error distribution and
# determine the properties of the sequenced clone
def determine_seq_clone_size_error(seq_clone_errors_freq, seq_clone_errors_vals):
    value_index = list(range(len(seq_clone_errors_vals)))
    seq_clone_index = np.random.choice(value_index, p=seq_clone_errors_freq)
    # First value is size of the clone
    clone_size = seq_clone_errors_vals[seq_clone_index][0]
    # Second value is number of mutated molecules in clone
    clone_error = seq_clone_errors_vals[seq_clone_index][1]
    return clone_size, clone_error

#
# Generate new sequence with a PCR mutation
#
def introduce_pcr_mutation(seq, rlen, insert_len):
    # Define forward and reverse read intervals within the sequence
    read_regions = list(range(0,rlen)) + list(range((insert_len - rlen), insert_len))
    # Choose a random position in sequence within the read regions
    pos = random.choice(read_regions)
    # Find alternative nucleotide for that position
    nuc = mutations_comb[seq[pos]][np.random.randint(3)]
    return seq[:pos] + nuc + seq[pos+1:]

#
# Iterate over a sample and simulate all reads
#
def process_sample(*args):
    if len(args) == 1:
        args = args[0]
    sample, args, err_probs, seq_clone_errors_freq, seq_clone_errors_vals = args

    # Open output files.
    reads1_f = gzip.open('{}/{}.1.fa.gz'.format(args.out_d, sample), 'wt')
    reads2_f = gzip.open('{}/{}.2.fa.gz'.format(args.out_d, sample), 'wt')

    # Read in the loci.
    loci = [] # [ (header0, seq0), (header1, seq1), ... ]
    header = None
    for line in gzip.open('{}/{}.fa.gz'.format(args.loci, sample), 'rt'):
        if line[0] == '>':
            header = line[1:-1]
        else:
            loci.append( ( header, line[:-1] ) )

    # Determine n_sequenced_reads for this sample
    n_sequenced_reads = target_reads(len(loci), args.cov)

    # Loop through number or target reads and generate all reads
    remaining_reads = n_sequenced_reads
    clone_i = 0
    while remaining_reads > 0:
        # Sample a random template to start a clone
        allele, insert_len = sample_a_template(loci, args.mean, args.stdev, args.rlen)
        base_name = build_base_template_name(allele, insert_len, args.rlen, clone_i)
        # Determine size of clone and number of reads with mutations
        clone_size, n_mut_reads = determine_seq_clone_size_error(seq_clone_errors_freq, seq_clone_errors_vals)
        # iterate over clone size and process sequence into reads
        duplicate_i = 0
        # write sequences without error
        for i in range(clone_size - n_mut_reads):
            fw_read, rev_read = sequence_read_pair(allele, insert_len, args.rlen, err_probs)
            reads1_f.write('>{}:{}/1\n{}\n'.format(base_name, duplicate_i+1, fw_read))
            reads2_f.write('>{}:{}/2\n{}\n'.format(base_name, duplicate_i+1, rev_read))
            duplicate_i += 1
        # write sequences with error
        if n_mut_reads > 0:
            mut_allele = (allele[0], introduce_pcr_mutation(allele[1], args.rlen, insert_len))
            for i in range(n_mut_reads):
                fw_read, rev_read = sequence_read_pair(mut_allele, insert_len, args.rlen, err_probs)
                reads1_f.write('>{}:{}/1\n{}\n'.format(base_name, duplicate_i+1, fw_read))
                reads2_f.write('>{}:{}/2\n{}\n'.format(base_name, duplicate_i+1, rev_read))
                duplicate_i += 1
        # Increate count of used clones
        clone_i += 1
        # Consume remaining reads
        remaining_reads -= clone_size

    #print('Sample \'{}\'...'.format(sample))
    #sys.stdout.flush()
    print('{} - Sample \'{}\': simulated {} read pairs'.format(elapsed(), sample, n_sequenced_reads))
    sys.stdout.flush()

#
# Main.
#
def main():

    # Parse command line options.
    p = argparse.ArgumentParser()
    p.add_argument('-l', '--loci-dir',   type=str,   dest='loci')
    p.add_argument('-o', '--out-dir',    type=str,   dest='out_d')
    p.add_argument('-c', '--coverage',   type=int,   dest='cov')
    p.add_argument('-r', '--read-len',   type=int,   dest='rlen')
    p.add_argument('-m', '--ins-mean',   type=int,   dest='mean')
    p.add_argument('-d', '--ins-stdv',   type=int,   dest='stdev')
    p.add_argument('-i', '--init-error', type=float, dest='ierr',     default=0.001)
    p.add_argument('-f', '--fin-error',  type=float, dest='ferr',     default=0.01)
    p.add_argument('-p', '--pcr-cycles', type=int,   dest='pcr_c',    default=None)
    p.add_argument('-n', '--ng-dna-tem', type=float, dest='dna_ng',   default=50.0)
    p.add_argument('-t', '--n-threads',  type=int,   dest='nthreads', default=1)

    args = p.parse_args()

    # Overwrite the help/usage behavior.
    p.format_usage = lambda : '''\
{prog} --loci-dir dir --out-dir dir --coverage int --read-len int --ins-mean int --ins-stdv int [--init-error float] [--fin-error float] [--pcr-cycles int] [--ng-dna-tem float] [--n-threads int]

Input/Output files:
    -l, --loci-dir: directory containing simulated RAD loci fasta
    -o, --out-dir: path to an output directory

Library preparation/sequencing:
    -c, --coverage: average per-locus coverage (X)
    -r, --read-len: read length (bp)
    -m, --ins-mean: insert length mean (bp)
    -d, --ins-stdv: insert length standard deviation (bp)

Additional options:
    -i, --init-error: sequencing error rate at the beginning (5\') of reads
    -f, --fin-error: sequencing error rate at the end (3\') of reads
    -p, --pcr-cycles: number of PCR cycles
    -n, --ng-dna-tem: DNA template (ng) for PCR reaction
    -t, --n-threads: number of threads

    '''.format(prog=program_name)
    p.format_help = p.format_usage

    for required in ['loci', 'out_d', 'cov', 'rlen', 'mean', 'stdev']:
        if args.__dict__[required] is None:
            print('Required argument \'{}\' is missing.\n'.format(required), file=sys.stderr)
            p.print_help()
            sys.exit(1)

    args.loci = args.loci.rstrip('/')
    args.out_d = args.out_d.rstrip('/')

    # Report parameters
    print(' '.join(sys.argv),
        ' ',
        'Extracting RAD Reads with parameters:',
        '  Coverage: {}X'.format(args.cov),
        '  Read Length: {}bp'.format(args.rlen),
        '  Insert Size: mean={}bp, sd={}bp'.format(args.mean, args.stdev),
        '  Sequencing Error: from {:.2%} to {:.2%}'.format(args.ierr, args.ferr),
        '  PCR cycles: {}'.format(args.pcr_c),
        '  DNA template for PCR: {}ng'.format(args.dna_ng),
        sep='\n')

    # Find samples.
    samples = []
    for f in os.listdir(args.loci):
        if f[:3] == 'msp':
            i = f.find('.')
            samples.append(f[:i])
    samples.sort()
    print('\n{} - Found {} samples.'.format(elapsed(), len(samples)))
    sys.stdout.flush()

    # Determine average number of loci for sequence-level distributions
    # Calculate average number of loci using 3 random samples
    avg_n_alleles = []
    for i in np.random.randint(0, len(samples), 3):
        in_file = '{}/{}.fa.gz'.format(args.loci, samples[i])
        with gzip.open(in_file) as one_sample:
            n_loci = len(one_sample.readlines()) // 2
            avg_n_alleles.append(n_loci)
    avg_n_alleles = int(np.average(avg_n_alleles))
    print('\n{} - Average number of alleles found per sample: {}'.format(elapsed(), avg_n_alleles))
    sys.stdout.flush()

    #
    # Process-wide variables
    #

    # Create the array of sequencing error thresholds.
    big_int = 1e9
    err_probs = (
        big_int,
        np.array([
            int( (args.ierr + (args.ferr - args.ierr) / (args.rlen - 1) * i) * big_int) for i in range(args.rlen)
            ], dtype=np.int64)
        )

    # If no PCR cycles, return empty PCR clone distribution, otherwise generate distribution
    seq_clone_errors_freq = None
    seq_clone_errors_vals = None
    if args.pcr_c == None:
        # If PCR cycles is None, only the (1, 0) clone is possible
        seq_clone_errors_vals = [(0, 0), (1, 0)]
        seq_clone_errors_freq = [0.0, 1.0]
    else:  
        # Amplified PCR duplicate distribution:
        pcr_model = lambda: simulate_pcr_inherited_efficiency(0.45, 0.2, args.pcr_c)
        ampl_clone_size_distrib = get_library_clone_size_distribution(pcr_model)
        print('\n{} - Generated amplified clone size distribution'.format(elapsed()))
        sys.stdout.flush()

        # Log convert amplified clone size distribution
        base = 2 ** 0.1
        log_ampl_clone_size_distrib = log_convert_ampl_clone_dist(ampl_clone_size_distrib, base)
        print('\n{} - Log converted amplified clone size distribution'.format(elapsed()))
        sys.stdout.flush()

        # Adjusted amplified PCR error distribution
        single_adj_error_distrib = adjust_dist_of_ampl_errors(log_ampl_clone_size_distrib, args.rlen, base, max_iter=100)
        print('\n{} - Merged amplified clone error distribution'.format(elapsed()))
        sys.stdout.flush()

        # Template molecules in PCR reaction
        n_templates = total_molecules_per_sample(args.mean, args.dna_ng, len(samples))

        # Amplified pool size in PCR reaction
        total_amplified_molecules = get_total_amplified_molecules(log_ampl_clone_size_distrib, n_templates, base)

        # Average number of sequenced reads across all samples
        n_sequenced_reads = target_reads(avg_n_alleles, args.cov)

        # Generate a single distribution that convined probabilities of duplicates and error for PCR clones
        seq_pcr_error_distrib = generate_pcr_dups_error_log_distrib(
            log_ampl_clone_size_distrib,
            n_sequenced_reads,
            total_amplified_molecules,
            base,
            single_adj_error_distrib)
        print('\n{} - Generated sequenced single clone size+error distribution'.format(elapsed()))
        sys.stdout.flush()

        # Convert the matrix into two lists (one of values and one of probabilities)
        seq_clone_errors_freq, seq_clone_errors_vals = get_seq_pcr_error_lists(seq_pcr_error_distrib)

    # Convert into single list of clone sizes and calculate PCR duplicates
    dup_distrib = get_duplicate_only_distrib(seq_clone_errors_freq, seq_clone_errors_vals)
    perc_duplicates = calculate_perc_duplicates(dup_distrib)
    print('\n{} - Formatted final clone duplicate/error distribution\n  Found {:.2%} of PCR duplicates'.format(elapsed(), perc_duplicates))
    sys.stdout.flush()

    # Write Sequenced clone size distribution to a file
    clone_dist = open('{}/sequenced_clone_distrib.tsv'.format(args.out_d), 'w')
    clone_dist.write('clone_size\tn_errors\tprob\n')
    for i in range(len(seq_clone_errors_freq)):
        clone_dist.write('{}\t{}\t{:.6g}\n'.format(seq_clone_errors_vals[i][0],
            seq_clone_errors_vals[i][1],
            seq_clone_errors_freq[i]))

    # Process all samples.
    print('\n{} - Simulating reads...\n'.format(elapsed()))
    sys.stdout.flush()
    multiprocessing.Pool(args.nthreads).map(
        process_sample,
        [(sample, args, err_probs, seq_clone_errors_freq, seq_clone_errors_vals) for sample in samples],
        chunksize=1
        )

    print('\n{} - Done.'.format(elapsed()))
    sys.stdout.flush()

if __name__ == '__main__':
    main()
